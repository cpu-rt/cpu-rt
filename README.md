## How to build and use cpu-rt library. 

### Checkout glibc source and build

$ mkdir /local/foo/cpurt

$ cd /local/foo/cpurt

$ mkdir cpurtinstall

$ git clone https://gitlab.com/cpu-rt/glibc.git

$ cd glibc

$ git checkout hjl/cpu-rt/master

$ mkdir ../build

$ cd ../build

$ ../glibc/configure --enable-hardcoded-path-in-tests --enable-cpu-rt --prefix=/local/foo/cpurt/cpurtinstall

$ make

$ make check # There may be some glibc failure and it's ok to proceed.

$ make install # It should put all the library in cpurtinstall diectory.

### Check cpu-rt library.

$ ls /local/foo/cpurt/cpurtinstall/lib/libcpu-rt-c.*

/local/foo/cpurt/cpurtinstall/lib/libcpu-rt-c.a  /local/foo/cpurt/cpurtinstall/lib/libcpu-rt-c.so

### How to use newly build cpu-rt library.

You can either link cpu-rt library manaully or use LD_PRELOAD.

$ export LD_PRELOAD=/local/foo/cpurt/cpurtinstall/lib/libcpu-rt-c.so

### Run application say foo.out, it should use latest libcpurt library.

$ ./foo.out

